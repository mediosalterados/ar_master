﻿using UnityEngine;
using System.Collections;

using GLIB.Extended;

namespace ARMediaCoca.Controllers.EventHandlers
{

    public class CapBehaviour : MonoBehaviour
    {

        public GameObject CallOutText;

        float _initialCallOutTextY;

        bool _callOutDisplayed = false;
        
        // Save for the first time the initial call out text y position
        void Awake() {
            if (CallOutText != null) {
                _initialCallOutTextY = CallOutText.transform.localPosition.y;
            }
        }

        // Use this for initialization
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void DisplayCallOutText() {
            if (CallOutText != null)
            {
                if (!_callOutDisplayed)
                {
                    CallOutText.SetActive(true);
                    
                    CallOutText.transform.localPosition = new Vector3(CallOutText.transform.localPosition.x, _initialCallOutTextY - 1f, CallOutText.transform.localPosition.z);
                    CallOutText.transform.localScale = new Vector3();

                    AnimateComponent animateComp = CallOutText.GetComponentInChildren<AnimateComponent>();

                    if (animateComp == null)
                        animateComp = CallOutText.AddComponent<AnimateComponent>();

                    animateComp.TranslateObject(new Vector3(CallOutText.transform.localPosition.x, _initialCallOutTextY, CallOutText.transform.localPosition.z), 0.5f, null);
                    animateComp.ScaleObject(new Vector3(0.7f, 0.7f, 0.7f), 0.5f, null);

                    _callOutDisplayed = true;
                }

            }
        }

        public void HideCallOutText() {
            if (CallOutText != null)
            {
                CallOutText.SetActive(false);
                _callOutDisplayed = false;
            }
        }
    }

}
