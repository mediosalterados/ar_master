﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System;

using GLIB.Extended;
using GLIB.Audio;
using GLIB.VFX;

//using ARMediaCoca.Controllers.Screens;
using ARMediaCoca.Views.Main.Screens;
using ARMediaCoca.Views.Main.Widgets;
using ARMediaCoca.Controllers.Services;
using ARMediaCoca.Controllers.EventHandlers;
using ARMediaCoca.Models.Interfaces;

namespace ARMediaCoca.Controllers.AR
{

    public class CokeStage : MonoBehaviour, IStage
    {

        Camera _mainCamera;

        public static CokeStage envase;

        string[] _contentJabon = new string[]{
            "s1suave", "s1antibacterial", "s1hair",
            "s4foam", "s1liquido", "sanitizante",
            "s11spray"
        };
        public Animator[] _cokeStageBottleAnimator;
        GameObject[] _contenedores;
        public string _mensajeTork = "";

        Vector3 _hiddenCapScale = new Vector3();
        Vector3 _displayedCapScale = new Vector3(0, 0, 0);

        bool _touchReleased = true;
        Action _onTouchRelease;
        Vector2 _lastTouchPosition;

        bool _isVisible = false;
        public bool isVisible
        {
            get { return _isVisible; }
        }

        bool _isOpened = false;
        public bool isOpened
        {
            get { return _isOpened; }
        }

        public bool isEnabled
        {
            get { return gameObject.activeSelf; }
        }

        OnStageOpenDelegate _onStageOpen;
        OnStageCloseDelegate _onStageClose;

        public OnStageOpenDelegate OnStageOpen
        {
            get { return _onStageOpen; }
            set { _onStageOpen = value; }
        }

        public OnStageCloseDelegate OnStageClose
        {
            get { return _onStageClose; }
            set { _onStageClose = value; }
        }


        // Use this for initialization
        void Start()
        {
            envase = this;
            _touchReleased = true;

            try
            {
                _cokeStageBottleAnimator = new Animator[_contentJabon.Length];
                _contenedores = new GameObject[_contentJabon.Length];

                for (int i = 0; i < _contentJabon.Length; i++)
                {
                    _contenedores[i] = this.Find(_contentJabon[i], this.gameObject);
                    _cokeStageBottleAnimator[i] = this.FindAndResolveComponent<Animator>(_contentJabon[i], this.gameObject);
                    _contenedores[i].SetActive(false);
                }

                _contenedores[0].SetActive(true);
                _cokeStageBottleAnimator[0].Play("Take 001");

                ScreenManager.Instance.CurrentSessionData.ActualModelin = 0;


                _mainCamera = Camera.main;

                bool errorFound = false;
                
                if ( _cokeStageBottleAnimator == null || _mainCamera == null)
                    errorFound = true;
                
                if (errorFound)
                    throw new Exception("Necessary game objects could not be found.");


            }
            catch (Exception error)
            {
                Debug.LogError("An Error Ocurred: " + error.Message);
                Debug.LogError(error.StackTrace);
            }

        }

        // Update is called once per frame
        void Update()
        {

            // Checks if we are not clicking any button component from the gui
            bool overGUIButton = false;

            if (EventSystem.current.currentSelectedGameObject != null) {
                Button pressedButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();

                if (pressedButton != null)
                    overGUIButton = true;
            }



            #region TouchEvents | MouseEvents
            // Mouse, change it to touch event
            if (Input.GetMouseButton(0) && !overGUIButton)
            {

                if (!_touchReleased || !ARScreen.Instance.isRunning)
                    return;

                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                RaycastHit hit = new RaycastHit();

                //Debug.Log("Mouse 0 triggered");

                _onTouchRelease = null;

                bool pointerOverGameObject = false;

                if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                        pointerOverGameObject = true;
                }
                else
                {
                    if (EventSystem.current.IsPointerOverGameObject())
                        pointerOverGameObject = true;
                }
                

                if (Physics.Raycast(ray, out hit) && !pointerOverGameObject)
                {

                    //Debug.Log("collision!");
                    Debug.Log(hit.collider.gameObject.name);

                    if(string.Equals(hit.collider.gameObject.name, "s1suave"))
                    {
                        HandleCapAction(TorkyScreen.Instance, "Liquid Soap \n Suave \n 70 05 21");
                    }
                    else if (string.Equals(hit.collider.gameObject.name, "s1antibacterial"))
                    {
                        HandleCapAction(TorkyScreen.Instance, "Liquid Soap \n Antibacterial \n 70 05 22");
                    }
                    else if (string.Equals(hit.collider.gameObject.name, "s1hair"))
                    {
                        HandleCapAction(TorkyScreen.Instance, "Liquid Soap \n Para cabello y cuerpo \n 40 00 13");
                    }
                    else if (string.Equals(hit.collider.gameObject.name, "s4foam"))
                    {
                        HandleCapAction(TorkyScreen.Instance, "Foam Soap \n Jabón Espumoso Extra Suave \n 40 12 11");
                    }
                    else if (string.Equals(hit.collider.gameObject.name, "s1liquido"))
                    {
                        HandleCapAction(TorkyScreen.Instance, "Liquid Soap \n Jabón Líquido \n 70 05 24");
                    }
                    else if (string.Equals(hit.collider.gameObject.name, "sanitizante"))
                    {
                        HandleCapAction(TorkyScreen.Instance, "Sanitizante Para Manos \n Gel Con Alcohol \n 70 05 25");
                    }
                    else if (string.Equals(hit.collider.gameObject.name, "s11spray"))
                    {
                        HandleCapAction(TorkyScreen.Instance, "Spray Soap \n Universal \n 62 05 01");
                    }

                }

                _touchReleased = false;

            }
            else
            {

                if (!_touchReleased && _onTouchRelease != null)
                {
                    _onTouchRelease();
                }
                _touchReleased = true;

            }

            #endregion
            
        }

        public void ShowBottle(int mov)
        {
            //mov 1 izq
            //mov 2 der
            _cokeStageBottleAnimator[ScreenManager.Instance.CurrentSessionData.ActualModelin].gameObject.ResolveComponent<AnimateComponent>().StopAllAnimations();
            _contenedores[ScreenManager.Instance.CurrentSessionData.ActualModelin].SetActive(false);
            if (mov == 2)
            {
                if (ScreenManager.Instance.CurrentSessionData.ActualModelin == 6)
                    ScreenManager.Instance.CurrentSessionData.ActualModelin = 0;
                else
                    ScreenManager.Instance.CurrentSessionData.ActualModelin ++;
            }
            else{
                if (ScreenManager.Instance.CurrentSessionData.ActualModelin == 0)
                    ScreenManager.Instance.CurrentSessionData.ActualModelin = 6;
                else
                    ScreenManager.Instance.CurrentSessionData.ActualModelin --;
            }

            if (TorkyScreen.Instance.isScreenRunning)
                ScreenManager.Instance.CloseScreen();

            _contenedores[ScreenManager.Instance.CurrentSessionData.ActualModelin].SetActive(true);
            _cokeStageBottleAnimator[ScreenManager.Instance.CurrentSessionData.ActualModelin].speed = 1;
            _cokeStageBottleAnimator[ScreenManager.Instance.CurrentSessionData.ActualModelin].Play("Take 001");

            ControlsScreen.Instance.Initialize();

        }

        public void DisplayStage()
        {

            gameObject.SetActive(true);

            _isVisible = true;

        }

        public void HideStage()
        {
        
            ScreenManager.Instance.CloseScreen();


            _isVisible = false;

            //ControlsScreen.Instance.Terminate();

        }

        public void OpenStage(Action onStageOpen = null)
        {
            //
        }

        public void CloseStage(Action onStageClose = null)
        {
            //
        }

        void HandleCapAction(IScreen screen, string mensaje)
        {
            _mensajeTork = mensaje;

            ScreenManager.Instance.stage = this;
            ScreenManager.Instance.DisplayScreen(screen);

            CokeStage.envase._cokeStageBottleAnimator[ScreenManager.Instance.CurrentSessionData.ActualModelin].speed = 1;
        }


    }
}
