﻿using UnityEngine;
using System.Collections;

using GLIB.Extended;
using GLIB.Utils;
using GLIB.Audio;
using GLIB.VFX;

using Vuforia;

using ARMediaCoca.Views.Main.Screens;

namespace ARMediaCoca.Controllers.Core {

    public class ARMediaCocaMain : MonoBehaviour {

        // Use this for initialization
        void Start() {
            ARScreen.Instance.Initialize();
            SoundModule.Instance.Initialize();

            AtlasManager.AddAtlas(ResourcesPaths.SPRITE_ATLAS);
            //NotificationSystem.Instance.TemplatePath = ResourcesPaths.TEMPLATE_GRIMOIRE_NOTIFICATIONSYSTEM;

        }

        // Update is called once per frame
        void Update() {



        }
    }

}