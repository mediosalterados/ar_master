﻿using UnityEngine;
using System.Collections;
using ARMediaCoca.Controllers.AR;
using ARMediaCoca.Views.Main.Screens;
using ARMediaCoca.Controllers.Services;

public class AnimEvent : MonoBehaviour
{
    AnimationClip anim;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Pausa()
    {
        //anim = CokeStage.envase._cokeStageBottleAnimator[ScreenManager.Instance.CurrentSessionData.ActualModelin].runtimeAnimatorController.animationClips[0];
        //Debug.Log(anim.name);
        CokeStage.envase._cokeStageBottleAnimator[ScreenManager.Instance.CurrentSessionData.ActualModelin].speed = 0;

        if (ControlsScreen.Instance.isRunning)
            ControlsScreen.Instance.Terminate();
    }
}
