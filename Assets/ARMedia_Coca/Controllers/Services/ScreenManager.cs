﻿namespace ARMediaCoca.Controllers.Services
{

    using UnityEngine;
    using System.Collections;
    using System.Reflection;

    using GLIB.Interface;
    using GLIB.Core;
    using System;

    using ARMediaCoca.Models.Interfaces;

    public class ScreenManager : BackModule<ScreenManager>
    {


        /*public delegate void OnScreenDisplayDelegate();
        private OnScreenDisplayDelegate _onScreenDisplay;

        public delegate void OnScreenCloseDelegate();
        private OnScreenCloseDelegate _onScreenClose;*/

        IStage _stage;
        IScreen _screen;
        IScreen _prevScreen;

        public IStage stage {
            get { return _stage; }
            set { _stage = value; }
        }

        protected override void ProcessInitialization()
        {
           
        }

        protected override void ProcessUpdate() {
            
           

        }

        protected override void ProcessTermination()
        {
            
        }

        public void DisplayScreen(IScreen screenToDisplay)
        {

            if (_stage == null)
            {
                Debug.LogError("There is not stage assigned, or is null");
                return;
            }

            else if (screenToDisplay == _screen) {
                Debug.Log("Requested to display an already displayed screen. Nothing to do.");
                return;
            }

            _prevScreen = _screen;
            _screen = screenToDisplay;
                                 
            if (!_stage.isOpened)
            {
               
                if (_prevScreen != null)
                    _prevScreen.EndScreen();

                if (_screen != null)
                    _screen.StartScreen();

                //_stage.OpenStage();

            }
            else {

                if (_prevScreen != null)
                    _prevScreen.EndScreen();

                //Debug.LogError("Screen is Running: " + _screen.isScreenRunning);

                _stage.CloseStage(
                    
                    delegate {

                        if (_screen != null)
                            _screen.StartScreen();

                        //_stage.OpenStage();

                    }
                );

            }

            
        }

        public void CloseScreen()
        {
            if (_stage == null || _screen == null)
            {
                Debug.LogError("There is not stage assigned, or is null");
                return;
            }

            _screen.EndScreen();

            _screen = null;

            //_onScreenClose = onScreenClose;

            //if(_stage != null)
                //_stage.CloseStage();

        }

        SessionData _currentSessionData;
        public SessionData CurrentSessionData
        {
            get
            {
                if (_currentSessionData == null)
                    _currentSessionData = new SessionData();

                return _currentSessionData;
            }
        }


    }

}