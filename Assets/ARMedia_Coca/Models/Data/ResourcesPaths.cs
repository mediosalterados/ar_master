﻿using UnityEngine;
using System.Collections;

public sealed class ResourcesPaths {

    private ResourcesPaths() { }

    public const float VOLUME_SFX_UI_CLICK = 0.6f;

    public const string OBJECT_WATERRINGS_GAMESTAGE = "Objects/GameStage";

    public const string PREFAB_RING = "Objects/Ring2";
    public const string PREFAB_PARTICLES_TIMERINGTRAIL = "Particles/TimeRingTrail";
    public const string PREFAB_PARTICLES_BADRINGTRAIL = "Particles/BadRingTrail";
    public const string PREFAB_PARTICLES_NORMALRINGTRAIL = "Particles/NormalRingTrail";
    public const string PREFAB_PARTICLES_RINGHIT = "Particles/RingHit";
    public const string PREFAB_PARTICLES_RINGEXPLOSION = "Particles/RingExplosion";
    public const string PREFAB_PARTICLES_CAPFIREWORKS = "Particles/CapFireworks";
    public const string PREFAB_PARTICLES_CAPHIT = "Particles/CapHit";

    public const string MAT_TIMERING = "Materials/TimeRing";
    public const string MAT_BADRING = "Materials/BadRing";
    public const string MAT_NORMALRING = "Materials/NormalRing";

    public const string SFX_WATER_PUSH = "Audio/waterPush";
    public const string SFX_GLASS_HIT = "Audio/glassHit";
    public const string SFX_BOTTLE_HIT = "Audio/bottleHit";
    public const string SFX_RING_DESTROY = "Audio/ringDestroy";
    public const string SFX_SCORE_UP = "Audio/scoreUp";
    public const string SFX_SCORE_DOWN = "Audio/scoreDown";
    public const string SFX_TIME_BONUS = "Audio/timeBonus";
    public const string SFX_GAME_RESULTS = "Audio/gameResults";
    public const string SFX_BOTTLE_POP = "Audio/bottlePop";
    public const string SFX_HIT_YES_SHORT = "Audio/hitYes";
    public const string SFX_HIT_YES = "Audio/hitYes2";
    public const string SFX_HIT_NO = "Audio/hitNo";
    public const string SFX_MENU_DISP_TICK = "Audio/menuDisplaceTick";
    public const string SFX_RESULTS_GOOD = "Audio/resultsGood";
    public const string SFX_RESULTS_BAD = "Audio/resultsBad";
    public const string SFX_RESULTS_LAUGH = "Audio/resultsLaugh";
    public const string SFX_UI_CLICK = "Audio/uiClick";

    public const string TEMPLATE_MAIN_ARSCREEN = "Templates/Main/Screens/ARScreen";
    public const string TEMPLATE_MAIN_CONTROLSSCREEN = "Templates/Main/Screens/ControlsScreen";
    public const string TEMPLATE_MAIN_ARLAYOUTWIDGET = "Templates/Main/Widgets/ARLayoutWidget";
    public const string TEMPLATE_MAIN_TORKYSCREEN = "Templates/Main/Screens/TorkyScreen";

    public const string TEMPLATE_GRIMOIRE_NOTIFICATIONSYSTEM = "Templates/Grimoire/NotificationSystem";

    public const string SPRITE_ATLAS = "Sprites/armedia_coca_atlas";

    public const string SPRITE_NAME_SODA_H = "b1_h";
    public const string SPRITE_NAME_BALLON_H = "b2_h";
    public const string SPRITE_NAME_COVER_H = "b3_h";
    public const string SPRITE_NAME_USB_H = "b4_h";
    public const string SPRITE_NAME_RADIO_H = "b5_h";
    public const string SPRITE_NAME_TICKET_H = "b6_h";

    public const string SPRITE_NAME_SODA_LARGE = "p_coke";
    public const string SPRITE_NAME_BALLON_LARGE = "p_balloon";
    public const string SPRITE_NAME_COVER_LARGE = "p_phone_cover";
    public const string SPRITE_NAME_USB_LARGE = "p_usb";
    public const string SPRITE_NAME_RADIO_LARGE = "p_radio";
    public const string SPRITE_NAME_TICKET_LARGE = "p_ticket";

    public const string BGM_1 = "Audio/BGM1";


}
