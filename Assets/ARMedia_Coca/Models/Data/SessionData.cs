﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Carry all values the user has modified in activities during a Session
/// </summary>
public class SessionData
{
    public enum ModelBottle
    {
        Botella600 = 0,
        Botella300 = 1
    }

    public const string BOTTLE_CLASICA = "pepsi_600";
    public const string BOTTLE_CHICA = "pepsi_300";

    public ModelBottle ActualModelo { get; set; }

    public int ActualModelin { get; set; }

}

