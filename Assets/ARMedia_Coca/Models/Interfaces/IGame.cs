﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARMediaCoca.Models.Interfaces
{
    public interface IGame
    {
        bool isRunning { get; }

        bool isPaused { get; }

        void Initialize();

        void Terminate();

        void PauseGame();

        void UnPauseGame();

    }
}
