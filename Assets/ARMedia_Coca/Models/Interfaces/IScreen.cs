﻿namespace ARMediaCoca.Models.Interfaces {

    using UnityEngine;
    using System.Collections;

    public interface IScreen {

        bool isScreenRunning { get;}
        void StartScreen();
        void EndScreen(bool force = false);

    }

}