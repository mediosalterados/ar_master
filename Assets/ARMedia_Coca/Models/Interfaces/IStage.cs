﻿using UnityEngine;
using System.Collections;
using System;

namespace ARMediaCoca.Models.Interfaces
{

    public delegate void OnStageOpenDelegate();
    public delegate void OnStageCloseDelegate();

    public interface IStage
    {
        OnStageOpenDelegate OnStageOpen { get; set; }
        OnStageCloseDelegate OnStageClose { get; set; }

        bool isVisible { get; }

        bool isOpened { get; }
        
        void DisplayStage();

        void HideStage();

        void OpenStage(Action OnStageOpen = null);

        void CloseStage(Action OnStageClose = null);

    }

}
