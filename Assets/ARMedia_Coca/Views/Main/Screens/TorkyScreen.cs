﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using GLIB.Interface;
using GLIB.Utils;
using GLIB.Extended;

using System;

using ARMediaCoca.Controllers.Services;
using ARMediaCoca.Models.Interfaces;

using ARMediaCoca.Controllers.AR;
using GLIB.Audio;

namespace ARMediaCoca.Views.Main.Screens
{

    public class TorkyScreen : UIModule<TorkyScreen>, IScreen
    {
        protected override string DisplayObjectPath
        {
            get
            {
                return ResourcesPaths.TEMPLATE_MAIN_TORKYSCREEN;
            }
        }

        protected override Transform DisplayObjectParent
        {
            get
            {

                /*GameObject parent = GameObject.Find("3DCanvas");

                if (parent)
                    return parent.transform;
                else*/
                return null;

            }
        }

        protected override Vector2? DisplayObjectPosition
        {
            get
            {
                return null;
            }
        }

        protected override int DisplayObjectZIndex
        {
            get
            {
                return (int)ZIndexPlacement.TOP;
            }
        }

        protected override Transition InOutTransition
        {
            get
            {
                return new Transition(Transition.InOutAnimations.SCALE);
            }
        }

        protected override void ProcessInitialization()
        {
            DisplayObject.transform.localRotation = Quaternion.identity;
            DisplayObject.transform.localPosition = new Vector3();

            Button closeButton = this.FindAndResolveComponent<Button>("Close<Button>", DisplayObject);
            closeButton.onClick.AddListener(delegate {
                ScreenManager.Instance.CloseScreen();
                SoundModule.Instance.PlaySFX(Resources.Load<AudioClip>(ResourcesPaths.SFX_UI_CLICK), -1, SoundChannel.PlayPriority.NORMAL, ResourcesPaths.VOLUME_SFX_UI_CLICK);
            });

            Text texto = this.FindAndResolveComponent<Text>("Label<Text>", DisplayObject);
            texto.text = CokeStage.envase._mensajeTork;

            Debug.Log("Tork Screen Initialized");

        }

        protected override void ProcessUpdate()
        {
            //_userPoints.text = User.USER_POINTS.ToString();
        }

        protected override void ProcessTermination()
        {
            Debug.Log("Tork Screen Terminated");
        }

        public bool isScreenRunning
        {
            get
            {
                return isRunning;
            }
        }

        public void StartScreen()
        {
            Initialize();
        }

        public void EndScreen(bool force = false)
        {
            Terminate(force);
        }



    }
}
